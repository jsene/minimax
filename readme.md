## MiniMax Algorithm

An implementation of the MiniMax algorithm for Tic Tac Toe. A player following the MiniMax algorithm is guaranteed to 
at worst draw. The game is played in the browser